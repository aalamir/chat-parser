package com.aalamir.chatparser.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.*;

/**
 * Created by a7med on 29/03/2017.
 */
public class WebUtilTest {
	private MockWebServer mMockServer;
	private String mBody = "<html> <head> <title>Assume well formatted html</title> </head> </html>";
	private String mUrl;

	@Before
	public void setUp() throws Exception {
		mMockServer = new MockWebServer();
		mMockServer.start();
		mUrl = mMockServer.url("/fakerequest").toString();
	}

	@After
	public void tearDown() throws Exception {
		mMockServer.shutdown();
	}

	@Test
	public void whenResponseIsProper_CorrectTitleIsFound() throws Exception {
		// correct header
		mMockServer.enqueue(buildResponseWithHeader(true, true));
		assertEquals("Assume well formatted html", Web.fetchPageTitle(mUrl));
	}

	@Test
	public void whenResponseIsMissingContentTypeHeader_UrlIsReturned() throws Exception {
		// missing header
		mMockServer.enqueue(buildResponseWithoutHeader(true));
		assertEquals(mUrl, Web.fetchPageTitle(mUrl));
	}

	@Test
	public void whenResponseHasWrongContentTypeHeader_UrlIsReturned() throws Exception {
		// wrong header
		mMockServer.enqueue(buildResponseWithHeader(false, true));
		assertEquals(mUrl, Web.fetchPageTitle(mUrl));
	}

	@Test
	public void whenResponseHasNoTitleTag_UrlIsReturned() throws Exception {
		// wrong header
		mMockServer.enqueue(buildResponseWithHeader(true, false));
		assertEquals(mUrl, Web.fetchPageTitle(mUrl));
	}

	private MockResponse buildResponseWithoutHeader(boolean addBody) {
		MockResponse response = new MockResponse();
		if (addBody) {
			response.setBody(mBody);
		}

		return response;
	}

	private MockResponse buildResponseWithHeader(boolean correctHeader, boolean addBody) {
		MockResponse response = new MockResponse();
		if (correctHeader) {
			response.addHeader("Content-Type", "text/html; charset=utf8");
		} else {
			response.addHeader("Content-Type", "application/json");
		}

		if (addBody) {
			response.setBody(mBody);
		}

		return response;
	}
}