package com.aalamir.chatparser.feature.parse.ui;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.aalamir.chatparser.feature.parse.ParseActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by a7med on 29/03/2017.
 *
 * This test is concerned with the ui not the actual parsing, thus no
 * data is used.
 */
@RunWith(AndroidJUnit4.class)
public class ParseActivityTest {

	@Rule
	public ActivityTestRule<ParseActivity> mActivityRule = new ActivityTestRule<>(ParseActivity.class);

	@Test
	public void whenIEnterMessage_ISeeCorrectJson() {
		// Type text and click send
		ParseActivityTestUtil.enterMessage("hello there");
		ParseActivityTestUtil.clickSend();

		// check that an empty json object is displayed
		ParseActivityTestUtil.assertResult("{}");
	}
}