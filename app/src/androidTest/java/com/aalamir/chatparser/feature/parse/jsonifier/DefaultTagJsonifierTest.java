package com.aalamir.chatparser.feature.parse.jsonifier;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import org.json.JSONArray;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Created by a7med on 29/03/2017.
 */
public class DefaultTagJsonifierTest {

	@Test
	public void jsonify() throws Exception {
		DefaultTagJsonifier jsonifier = new DefaultTagJsonifier();
		Task<JSONArray> task = jsonifier.jsonify(Arrays.asList("item1", "item2"));
		JSONArray array = Tasks.await(task);

		assertNotNull(array);
		assertEquals(2, array.length());
		assertEquals("item1", array.getString(0));
		assertEquals("item2", array.getString(1));
	}

}