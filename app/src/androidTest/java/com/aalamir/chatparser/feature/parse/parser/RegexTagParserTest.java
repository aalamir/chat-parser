package com.aalamir.chatparser.feature.parse.parser;

import com.aalamir.chatparser.feature.parse.Constants;
import com.google.android.gms.tasks.Tasks;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by a7med on 28/03/2017.
 */
public class RegexTagParserTest {
	private final String mText = "@pal, have you seen https://www.google.com/cool? (grin) (thumbsup). Please ignore mail@example.com";
	private final TagParser mParser = new RegexTagParser();

	@Test
	public void parseMentions() throws Exception {
		List<String> mentions = Tasks.await(mParser.parse(Constants.MENTION_PATTERN, mText));
		assertNotNull(mentions);
		assertEquals(1, mentions.size());
		assertEquals("pal", mentions.get(0));
	}

	@Test
	public void parseEmoticons() throws Exception {
		List<String> emoticons = Tasks.await(mParser.parse(Constants.EMOTICON_PATTERN, mText));
		assertNotNull(emoticons);
		assertEquals(2, emoticons.size());
		assertEquals("grin", emoticons.get(0));
		assertEquals("thumbsup", emoticons.get(1));
	}

	@Test
	public void parseLinks() throws Exception {
		List<String> links = Tasks.await(mParser.parse(Constants.LINK_PATTERN, mText));
		assertNotNull(links);
		assertEquals(1, links.size());
		assertEquals("https://www.google.com/cool", links.get(0));
	}
}