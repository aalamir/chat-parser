package com.aalamir.chatparser.feature.parse.jsonifier;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.*;

/**
 * Created by a7med on 29/03/2017.
 */
public class LinkTagJsonifierTest {
	private MockWebServer mMockServer;
	private String mBody = "<html> <head> <title>Assume well formatted html</title> </head> </html>";
	private String mUrl;

	@Before
	public void setUp() throws Exception {
		mMockServer = new MockWebServer();
		mMockServer.enqueue(new MockResponse().setBody(mBody).addHeader("Content-Type", "text/html"));
		mMockServer.start();
		mUrl = mMockServer.url("/fakerequest").toString();
	}

	@After
	public void tearDown() throws Exception {
		mMockServer.shutdown();
	}

	@Test
	public void jsonify() throws Exception {
		LinkTagJsonifier jsonifier = new LinkTagJsonifier();
		Task<JSONArray> task = jsonifier.jsonify(Arrays.asList(mUrl));
		JSONArray array = Tasks.await(task);

		assertNotNull(array);
		assertEquals(1, array.length());

		JSONObject link = array.getJSONObject(0);

		assertNotNull(link);
		assertTrue(link.has("title"));
		assertTrue(link.has("url"));
		assertEquals("Assume well formatted html", link.getString("title"));
		assertEquals(mUrl, link.getString("url"));
	}

}