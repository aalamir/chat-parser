package com.aalamir.chatparser.feature.parse.ui;

import com.aalamir.chatparser.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by a7med on 29/03/2017.
 */

public class ParseActivityTestUtil {

	static void enterMessage(String message) {
		onView(withId(R.id.edit_chat_box)).perform(typeText(message), closeSoftKeyboard());
	}

	static void clickSend() {
		onView(withId(R.id.btn_send)).perform(click());
	}

	static void assertResult(String result) {
		onView(withId(R.id.edit_result)).check(matches(withText(result)));
	}
}
