package com.aalamir.chatparser.feature.parse.jsonifier;

import com.google.android.gms.tasks.Task;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by a7med on 26/03/2017.
 *
 * Defines the basic functionality for a Jsonifier; converting tags into a JSON-array.
 */

public interface TagJsonifier {

	Task<JSONArray> jsonify(final List<String> tags);
}
