package com.aalamir.chatparser.feature.parse.parser;

import com.aalamir.chatparser.util.ExecutorProvider;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a7med on 26/03/2017.
 *
 * An implementation of {@link TagParser} using Regex.
 */

public class RegexTagParser implements TagParser {

	/**
	 * Extract patterns defined in tag from text.
	 * @param tag     Must be a Regex pattern.
	 * @param text    Target text to parse.
	 * @return All found matching tags.
	 */
	@Override
	public Task<List<String>> parse(final String tag, final String text) {

		// encapsulate parse result in a Task
		return Tasks.call(ExecutorProvider.getExecutor(), new Callable<List<String>>() {
			@Override
			public List<String> call() throws Exception {
				return doParse(tag, text);
			}
		});
	}

	/**
	 * This is where the actual parsing happens.
	 */
	private List<String> doParse(String tag, String text) {
		Pattern pattern = Pattern.compile(tag);
		Matcher matcher = pattern.matcher(text);

		List<String> found = new ArrayList<>();
		while (matcher.find()) {
			// extract group content
			found.add(matcher.group(1));
		}

		return found;
	}
}
