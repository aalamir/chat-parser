package com.aalamir.chatparser.feature.parse.parser;

import com.google.android.gms.tasks.Task;

import java.util.List;

/**
 * Created by a7med on 26/03/2017.
 *
 * Defines the basic functionality for a parser; it looks for a tag
 * in a given text and returns all found occurrences.
 */

public interface TagParser {

	Task<List<String>> parse(String tag, String text);
}
