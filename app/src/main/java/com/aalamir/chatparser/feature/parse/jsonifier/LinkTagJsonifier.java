package com.aalamir.chatparser.feature.parse.jsonifier;

import com.aalamir.chatparser.util.ExecutorProvider;
import com.aalamir.chatparser.util.Web;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


/**
 * Created by a7med on 26/03/2017.
 *
 * Implementation of {@link TagJsonifier} for link tags that requires extra info retrieval.
 */

public class LinkTagJsonifier implements TagJsonifier {

	private final static String KEY_URL = "url";
	private final static String KEY_TITLE = "title";

	@Override
	public Task<JSONArray> jsonify(final List<String> tags) {

		// wrap the result in a Task
		return Tasks.call(ExecutorProvider.getExecutor(), new Callable<JSONArray>() {
			@Override
			public JSONArray call() throws Exception {
				return doJsonify(tags);
			}
		});
	}

	/**
	 * Try to connect to specified url and retrieve the page title; the result is then
	 * converted to a JSONArray.
	 */
	private JSONArray doJsonify(List<String> tags) {

		JSONArray array = new JSONArray();

		for (String tag : tags) {

			Map<String, String> attributes = new HashMap<>();
			attributes.put(KEY_URL, tag);
			attributes.put(KEY_TITLE, Web.fetchPageTitle(tag));

			array.put(new JSONObject(attributes));
		}

		return array;
	}
}
