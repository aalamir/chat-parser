package com.aalamir.chatparser.feature.parse.processor;

import com.aalamir.chatparser.feature.parse.parser.TagParser;
import com.aalamir.chatparser.feature.parse.jsonifier.TagJsonifier;

/**
 * Created by a7med on 29/03/2017.
 *
 * Specify for a given tag which {@link TagParser} and {@link TagJsonifier} to use
 * as well as the title for the parsing result.
 */

public class ParseRule {
	private String mTag;
	private TagParser mParser;
	private TagJsonifier mJsonifier;
	private String mTitle;

	public ParseRule(String tag, TagParser parser, TagJsonifier jsonifier, String title) {
		mTag = tag;
		mParser = parser;
		mJsonifier = jsonifier;
		mTitle = title;
	}

	public String getTag() {
		return mTag;
	}

	public TagParser getParser() {
		return mParser;
	}

	public TagJsonifier getJsonifier() {
		return mJsonifier;
	}

	public String getTitle() {
		return mTitle;
	}
}
