package com.aalamir.chatparser.feature.parse;


/**
 * Created by a7med on 27/03/2017.
 *
 * Constants used for Parser/Processor.
 */

public class Constants {
	// Titles are used by ParseProcessor to name the corresponding JSONArray in the final
	// result.
	public static final String MENTION_TITLE = "mentions";
	public static final String LINK_TITLE = "links";
	public static final String EMOTICON_TITLE = "emoticons";

	// Patterns are consumed by RegexTagParser, the relevant part of the pattern
	// is grouped for easier extraction.
	public static final String EMOTICON_PATTERN = "\\(([a-zA-Z0-9]{1,15})\\)";
	public static final String MENTION_PATTERN = "(?:^|[^\\w])@([\\w_-]+)";
	public static final String LINK_PATTERN = "(https?://(?:www\\.)?[-a-zA-Z0-9]{2,256}\\.[a-z]{2,6}[a-zA-Z0-9/]+)";
}
