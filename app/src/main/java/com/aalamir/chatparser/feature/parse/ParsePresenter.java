package com.aalamir.chatparser.feature.parse;

import com.aalamir.chatparser._common.BasePresenter;
import com.aalamir.chatparser._common.Ui;
import com.aalamir.chatparser.feature.parse.jsonifier.DefaultTagJsonifier;
import com.aalamir.chatparser.feature.parse.jsonifier.LinkTagJsonifier;
import com.aalamir.chatparser.feature.parse.parser.RegexTagParser;
import com.aalamir.chatparser.feature.parse.processor.ParseProcessor;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by a7med on 26/03/2017.
 */
class ParsePresenter
		extends BasePresenter<ParsePresenter.ParseUi>
		implements ParseProcessor.ParsePipelineListener {

	private final static int JSON_INDENT = 2;

	private ParseProcessor mParseProcessor;

	ParsePresenter(ParseUi ui) {
		super(ui);
	}

	@Override
	public void create() {
		mParseProcessor = new ParseProcessor();

		// add rule for parsing mentions
		mParseProcessor.addRule(Constants.MENTION_PATTERN,
				new RegexTagParser(),
				new DefaultTagJsonifier(),
				Constants.MENTION_TITLE);

		// add rule for parsing emoticons
		mParseProcessor.addRule(Constants.EMOTICON_PATTERN,
				new RegexTagParser(),
				new DefaultTagJsonifier(),
				Constants.EMOTICON_TITLE);

		// add rule for parsing links
		mParseProcessor.addRule(Constants.LINK_PATTERN,
				new RegexTagParser(),
				new LinkTagJsonifier(),
				Constants.LINK_TITLE);
	}

	@Override
	public void resume() {
		super.resume();

		// app is back to foreground, listen for parsing results
		mParseProcessor.setListener(this);
	}

	@Override
	public void pause() {
		super.pause();

		// app is no longer visible, stop listening for updates
		mParseProcessor.setListener(null);
	}

	void onSendClicked(String text) {
		getUi().hideKeyboard();
		getUi().showLoadingState();

		mParseProcessor.process(text);
	}

	@Override
	public void onParsingSuccess(JSONObject result) {
		getUi().hideLoadingState();

		// a little hack, JSONObject.toString() is JavaScript compliant and escapes
		// slashes; unescape them to make them easier on the eyes
		try {
			String indentedJson = result.toString(JSON_INDENT).replace("\\/", "/");
			getUi().displayResult(indentedJson);
		} catch (JSONException e) {
			getUi().showError(e.getLocalizedMessage());
		}
	}

	@Override
	public void onParsingError(String error) {
		getUi().hideLoadingState();
		getUi().showError(error);
	}

	/**
	 * Expected functionality for a UI that is attached to {@link ParsePresenter}
	 */
	interface ParseUi extends Ui {
		void displayResult(String result);
	}
}
