package com.aalamir.chatparser.feature.parse;

import android.os.Bundle;
import android.widget.EditText;

import com.aalamir.chatparser.R;
import com.aalamir.chatparser._common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by a7med on 26/03/2017.
 */
public class ParseActivity
		extends BaseActivity<ParsePresenter>
		implements ParsePresenter.ParseUi {

	@BindView(R.id.edit_chat_box)
	EditText mChatBox;

	@BindView(R.id.edit_result)
	EditText mResult;

	@OnClick(R.id.btn_send)
	void onSendClicked() {
		mPresenter.onSendClicked(mChatBox.getText().toString());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parse);
		ButterKnife.bind(this);
	}

	@Override
	protected void initPresenter() {
		mPresenter = new ParsePresenter(this);
	}

	@Override
	public void displayResult(String result) {
		mResult.setText(result);
	}
}
