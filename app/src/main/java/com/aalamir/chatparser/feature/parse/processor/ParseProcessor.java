package com.aalamir.chatparser.feature.parse.processor;

import android.support.annotation.NonNull;

import com.aalamir.chatparser.feature.parse.parser.TagParser;
import com.aalamir.chatparser.feature.parse.jsonifier.TagJsonifier;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by a7med on 27/03/2017.
 *
 * Glues together {@link TagParser} and {@link TagJsonifier} to parse and process
 * a given text and provide feedback to listener after all is done. The final result
 * is a JSON object whose attributes are the JSONArrays retrieved from {@link TagJsonifier}.
 */

public class ParseProcessor {

	// processing is multithreaded, this counter is used to track how many
	// callbacks were received
	private AtomicInteger mProcessCount;

	private List<ParseRule> mRules;

	private ParsePipelineListener mListener;

	private final JSONObject mResult;

	public ParseProcessor() {
		mProcessCount = new AtomicInteger();
		mResult = new JSONObject();
		mRules = new ArrayList<>();
	}

	public void setListener(ParsePipelineListener listener) {
		mListener = listener;
	}

	/**
	 * Specify the {@link TagParser} and {@link TagJsonifier} to use for a given {@code tag}. The result
	 * will be added to the final JSONObject under {@code title}.
	 */
	public void addRule(String tag, TagParser parser, TagJsonifier jsonifier, String title) {
		mRules.add(new ParseRule(tag, parser, jsonifier, title));
	}

	public void clearRules() {
		mRules.clear();
	}

	private void reset() {
		// remove previously found tags
		Iterator keys = mResult.keys();
		while(keys.hasNext())
			mResult.remove(keys.next().toString());

		// reset finished processing counter
		mProcessCount.set(0);
	}

	/**
	 * Process {@code text} using previously added rules.
	 */
	public void process(String text) {
		// clear previous results
		synchronized (mResult) {
			reset();
		}

		for (ParseRule rule : mRules) {
			final String tag = rule.getTag();
			final TagParser parser = rule.getParser();
			final TagJsonifier jsonifier = rule.getJsonifier();
			final String title = rule.getTitle();

			// first parse the text for tags
			parser.parse(tag, text).continueWithTask(new Continuation<List<String>, Task<JSONArray>>() {

				@Override
				public Task<JSONArray> then(@NonNull Task<List<String>> task) throws Exception {
					// then jsonify found tags
					return jsonifier.jsonify(task.getResult());
				}

			}).addOnCompleteListener(new OnCompleteListener<JSONArray>() {

				@Override
				public void onComplete(@NonNull Task<JSONArray> task) {
					// when all is done
					synchronized (mResult) {
						try {
							// add result under given title
							JSONArray array = task.getResult();
							if (array.length() > 0) {
								mResult.put(title, array);
							}
						} catch (JSONException e) {
							if (mListener != null) {
								mListener.onParsingError(e.getLocalizedMessage());
							}
						}
					}

					// if all processes have come back, report to listener
					if (mProcessCount.incrementAndGet() == mRules.size() && mListener != null) {
						mListener.onParsingSuccess(mResult);
					}
				}
			});
		}

	}

	public interface ParsePipelineListener {
		void onParsingSuccess(JSONObject result);
		void onParsingError(String error);
	}
}
