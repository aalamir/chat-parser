package com.aalamir.chatparser.feature.parse.jsonifier;

import com.aalamir.chatparser.util.ExecutorProvider;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import org.json.JSONArray;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by a7med on 26/03/2017.
 *
 * Implementation of {@link TagJsonifier} for basic tags requiring no special processing.
 */

public class DefaultTagJsonifier implements TagJsonifier {

	@Override
	public Task<JSONArray> jsonify(final List<String> tags) {

		// wrap the result in a Task
		return Tasks.call(ExecutorProvider.getExecutor(), new Callable<JSONArray>() {
			@Override
			public JSONArray call() throws Exception {
				return doJsonify(tags);
			}
		});
	}

	/**
	 * This is where the actual conversion to JSONArray happens.
	 */
	private JSONArray doJsonify(final List<String> tags) {
		JSONArray array = new JSONArray();
		for (String tag : tags) {
			array.put(tag);
		}

		return array;
	}
}
