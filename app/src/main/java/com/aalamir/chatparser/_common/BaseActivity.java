package com.aalamir.chatparser._common;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.aalamir.chatparser.R;


/**
 * Created by a7med on 26/03/2017.
 *
 * Implements common functionality for an activity. All activities should
 * extend this class.
 */
public abstract class BaseActivity<T extends Presenter>
		extends AppCompatActivity
		implements Ui {

	protected T mPresenter;

	protected ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initPresenter();
		mPresenter.create();
	}

	protected abstract void initPresenter();

	@Override
	public void onResume() {
		super.onResume();
		if (mPresenter != null) {
			mPresenter.resume();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mPresenter != null) {
			mPresenter.pause();
		}
	}

	@Override
	public void showError(String message) {
		new AlertDialog.Builder(this)
				.setTitle(R.string.error_title)
				.setMessage(message)
				.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				})
				.show();
	}


	@Override
	public void showLoadingState() {
		if (mProgressDialog == null) {
			mProgressDialog = new ProgressDialog(this);
		}

		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.setCanceledOnTouchOutside(false);

		mProgressDialog.show();
	}

	@Override
	public void hideLoadingState() {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	@Override
	public void hideKeyboard() {
		View view = getCurrentFocus();
		if (view != null) {
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}
}
