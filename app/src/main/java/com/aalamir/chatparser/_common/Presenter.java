package com.aalamir.chatparser._common;

/**
 * Created by a7med on 26/03/2017.
 *
 * Basic interface for a presenter; provides methods to correspond to
 * ui lifecycle.
 */
interface Presenter {
	void create();

	void resume();
	void pause();
}
