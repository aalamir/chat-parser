package com.aalamir.chatparser._common;

import android.support.annotation.StringRes;

/**
 * Created by a7med on 26/03/2017.
 *
 * Basic interface for a ui. Provides methods commonly required by presenters.
 */
public interface Ui {

	void showError(String message);

	void showLoadingState();
	void hideLoadingState();

	void hideKeyboard();
}
