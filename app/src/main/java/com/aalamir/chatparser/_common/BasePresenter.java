package com.aalamir.chatparser._common;

import android.support.annotation.CallSuper;

/**
 * Created by a7med on 26/03/2017.
 *
 * Implements common functionality for a presenter. All presenters should
 * extend this class.
 */
public abstract class BasePresenter<T extends Ui>
        implements Presenter {

    private final T mUi;
    private boolean mPaused;

    public BasePresenter(T ui) {
        this.mUi = ui;
    }

    @CallSuper
    @Override
    public void resume() {
        mPaused = false;
    }

    @CallSuper
    @Override
    public void pause() {
        mPaused = true;
    }

    protected boolean isPaused() {
        return mPaused;
    }

    protected T getUi() {
        return mUi;
    }
}
