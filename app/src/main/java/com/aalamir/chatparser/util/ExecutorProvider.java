package com.aalamir.chatparser.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by a7med on 28/03/2017.
 *
 * Provides a {@link ThreadPoolExecutor}.
 */

public class ExecutorProvider {
	private static int THREADS_PER_CORE = 2;
	private static long THREAD_LIFETIME = 60L;

	private static ThreadPoolExecutor sExecutor;

	public static ThreadPoolExecutor getExecutor() {
		if (sExecutor == null) {
			int numCores = Runtime.getRuntime().availableProcessors();
			sExecutor = new ThreadPoolExecutor(numCores * THREADS_PER_CORE, numCores * THREADS_PER_CORE,
					THREAD_LIFETIME, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		}

		return sExecutor;
	}
}
