package com.aalamir.chatparser.util;

import android.webkit.MimeTypeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a7med on 29/03/2017.
 *
 * Web related utilities.
 */

public class Web {

	private static final String HTML_TITLE_PATTERN = "\\<title>(.*)\\</title>";
	private static final String MIME_HTML_PATTERN = "(text\\/html)[;\\s]?";
	private static final String HTML_HEAD_CLOSE = "</head>";

	private static final String USER_AGENT_KEY = "User-Agent";
	private static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36" +
			"(KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

	/**
	 * Retrieve the title of a web page specified by {@code url}.
	 *
	 * There is some tolerance for malformed HTML. However finding the title of a
	 * seriously malformed page is unlikely.
	 */
	public static String fetchPageTitle(String url) {
		// match the <title> section of the web page, ignore case to increase the probability
		// of a match.
		Pattern titlePattern = Pattern.compile(HTML_TITLE_PATTERN, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

		try {
			// fake user agent, some websites wouldn't give a proper title otherwise
			URLConnection connection = new URL(url).openConnection();
			connection.addRequestProperty(USER_AGENT_KEY, USER_AGENT_VALUE);
			connection.connect();

			// only html files are considered
			if (isContentTypeHtml(connection)) {
				InputStream in = connection.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));

				StringBuilder html = new StringBuilder();
				for (String line; (line = reader.readLine()) != null; ) {
					html.append(line);

					// title is found, extract it
					Matcher matcher = titlePattern.matcher(html);
					if (matcher.find()) {
						return matcher.group(1);
					}

					// a bit of optimization, if the </head> tag is reached without find <title>
					// ignore the rest of the page
					if (line.contains(HTML_HEAD_CLOSE)) {
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return url;
	}


	/**
	 * Attempt to find the Content-Type header of the passed connection
	 * and match it against text/html type.
	 */
	private static boolean isContentTypeHtml(URLConnection conn) {
		Pattern textHtml = Pattern.compile(MIME_HTML_PATTERN, Pattern.CASE_INSENSITIVE);

		Map<String, List<String>> headers = conn.getHeaderFields();
		if (headers.containsKey("Content-Type")) {
			List<String> values = headers.get("Content-Type");

			for (String value : values) {
				Matcher matcher = textHtml.matcher(value);
				if(matcher.find()) {
					return true;
				}
			}
		}

		return false;
	}
}
